package Assignments.Eleven;

public class StringToArray {
    public static void main(String[] args) {
        String string="I am a good girl";
        for(int i=0;i<string.length();i++)
        {
            char ch=string.charAt(i);
            System.out.print(ch);
        }

        System.out.println();

        char[] chArray=string.toCharArray();
        for(char c:chArray)
        {
            System.out.print(c+",");
        }
    }
}


/*OUTPUT
I am a good girl
I, ,a,m, ,a, ,g,o,o,d, ,g,i,r,l,
 */