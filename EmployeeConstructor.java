package Assignments.Eleven;

public class EmployeeConstructor {
    int empId;
    String empName;
    String address;
    float salary;

    EmployeeConstructor()
    {

    }

    public EmployeeConstructor(int empId, String empName, String address, float salary) {
        this.empId = empId;
        this.empName = empName;
        this.address = address;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "EmployeeConstructor{" +
                "empId=" + empId +
                ", empName='" + empName + '\'' +
                ", address='" + address + '\'' +
                ", salary=" + salary +
                '}';
    }

    public static void main(String[] args) {

        EmployeeConstructor employee1=new EmployeeConstructor();
        EmployeeConstructor employee2=new EmployeeConstructor(12,"Rahul Vaidya","MG road",98652.95f);
        System.out.println(employee1);
        System.out.println(employee2);
    }

}


/*
OUTPUT
EmployeeConstructor{empId=0, empName='null', address='null', salary=0.0}
EmployeeConstructor{empId=12, empName='Rahul Vaidya', address='MG road', salary=98652.95}
 */