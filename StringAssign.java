package Assignments.Eleven;

import java.util.Arrays;
import java.util.Scanner;

public class StringAssign {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in) ;
        String[] StringArray = {"sanyami","praveer","Ayushman","Aman","ashraf"};
        String temp;


        for(int i=0;i<StringArray.length-1;i++)
        {
            for(int j=i+1;j<StringArray.length;j++)
            {
                if(StringArray[i].length()>StringArray[j].length())
                {
                    temp=StringArray[i];
                    StringArray[i]=StringArray[j];
                    StringArray[j]=temp;
                }

            }
        }

        System.out.println(Arrays.toString(StringArray));

    }
}


/*
output
[Aman, ashraf, sanyami, praveer, Ayushman]
 */