package Assignments.Eleven;

public class OperandsAndOperators {
    public static void main(String[] args) {
       String expression= "3+(20%2)*(20/2)";

       char[] ex =expression.toCharArray();
       System.out.println("The tokens are");
       for(char c:ex)
       {
           System.out.print(c+",");
       }

        System.out.println();
        System.out.println("The operators are as follows");
        for(char c:ex)
        {
            if(c=='+'|| c=='('||c=='%'||c==')'||c=='*'||c=='/')
            System.out.print(c+",");
        }
        System.out.println();
        System.out.println("The operands are as follows");
        for(char c:ex)
        {
            if(c!='+'&& c!='('&&c!='%'&&c!=')'&&c!='*'&&c!='/')
                System.out.print(c+",");
        }

    }
}



/*
OUTPUT
The tokens are
3,+,(,2,0,%,2,),*,(,2,0,/,2,),
The operators are as follows
+,(,%,),*,(,/,),
The operands are as follows
3,2,0,2,2,0,2,
 */