package Assignments.Eleven;

import java.util.Arrays;
import java.util.StringTokenizer;

public class Delimiters {
    public static void main(String[] args) {

        String value = "Hii,all.How are you?";
        StringTokenizer stringTokenizer=new StringTokenizer(value,", . ?",true);

        while(stringTokenizer.hasMoreTokens())
        {
            System.out.println(stringTokenizer.nextToken());
        }
    }
}



/*
OUTPUT
Hii
,
all
.
How

are

you
?

 */